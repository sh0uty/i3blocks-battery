#!/usr/bin/env python3

from psutil import sensors_battery
import fontawesome as fa

def main():
    battery = sensors_battery()
    percent = battery.percent
    time_left = battery.secsleft
    plugged = battery.power_plugged

    output = ''
    output += fa.icons['battery-full']

    output += '  ' + str(round(percent)) + '%'

    if plugged:
        output += ' CHA'
    else:
        output += ' DIS'

    print(output)


if __name__ == '__main__':
    main()
